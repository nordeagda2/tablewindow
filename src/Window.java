import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOError;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class Window {

	private JFrame frame;
	private JTextField fieldTytul;
	private JTextField fieldRezyser;
	private JTextField fieldRok;
	private JTextField fieldGatunek;
	private JTextField fieldOcena;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private MovieTableModel model;
	private int zaznaczonyIndex;
	private JButton remove;

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();

		model = new MovieTableModel();

		table.setModel(model);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		ListSelectionModel modelZaznaczania = table.getSelectionModel();
		modelZaznaczania.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				zaznaczonyIndex = table.getSelectedRow();
				if (zaznaczonyIndex == -1) {
					remove.setEnabled(false);
				} else {
					remove.setEnabled(true);
				}
				System.out.println("Zaz:" + zaznaczonyIndex);
			}
		});

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 724, 464);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		JPanel panelGrid = new JPanel();
		GridBagConstraints gbc_panelGrid = new GridBagConstraints();
		gbc_panelGrid.fill = GridBagConstraints.BOTH;
		gbc_panelGrid.gridx = 0;
		gbc_panelGrid.gridy = 0;
		frame.getContentPane().add(panelGrid, gbc_panelGrid);
		panelGrid.setLayout(new GridLayout(0, 2, 0, 0));

		JPanel panelLeft = new JPanel();
		panelLeft.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelGrid.add(panelLeft);
		GridBagLayout gbl_panelLeft = new GridBagLayout();
		gbl_panelLeft.columnWidths = new int[] { 0, 0 };
		gbl_panelLeft.rowHeights = new int[] { 0, 0, 0 };
		gbl_panelLeft.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelLeft.rowWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		panelLeft.setLayout(gbl_panelLeft);

		JPanel panelLeftTop = new JPanel();
		GridBagConstraints gbc_panelLeftTop = new GridBagConstraints();
		gbc_panelLeftTop.insets = new Insets(0, 0, 5, 0);
		gbc_panelLeftTop.fill = GridBagConstraints.BOTH;
		gbc_panelLeftTop.gridx = 0;
		gbc_panelLeftTop.gridy = 0;
		panelLeft.add(panelLeftTop, gbc_panelLeftTop);
		GridBagLayout gbl_panelLeftTop = new GridBagLayout();
		gbl_panelLeftTop.columnWidths = new int[] { 0, 0 };
		gbl_panelLeftTop.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_panelLeftTop.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelLeftTop.rowWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		panelLeftTop.setLayout(gbl_panelLeftTop);

		JLabel lblNewLabel = new JLabel("Dane filmu:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panelLeftTop.add(lblNewLabel, gbc_lblNewLabel);

		JPanel panelTytul = new JPanel();
		GridBagConstraints gbc_panelTytul = new GridBagConstraints();
		gbc_panelTytul.insets = new Insets(0, 0, 5, 0);
		gbc_panelTytul.fill = GridBagConstraints.BOTH;
		gbc_panelTytul.gridx = 0;
		gbc_panelTytul.gridy = 1;
		panelLeftTop.add(panelTytul, gbc_panelTytul);
		panelTytul.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblTytul = new JLabel("Tytuł:");
		panelTytul.add(lblTytul);

		fieldTytul = new JTextField();
		panelTytul.add(fieldTytul);
		fieldTytul.setColumns(10);

		JPanel panelRezyser = new JPanel();
		GridBagConstraints gbc_panelRezyser = new GridBagConstraints();
		gbc_panelRezyser.insets = new Insets(0, 0, 5, 0);
		gbc_panelRezyser.fill = GridBagConstraints.BOTH;
		gbc_panelRezyser.gridx = 0;
		gbc_panelRezyser.gridy = 2;
		panelLeftTop.add(panelRezyser, gbc_panelRezyser);
		panelRezyser.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel_1 = new JLabel("Reżyser:");
		panelRezyser.add(lblNewLabel_1);

		fieldRezyser = new JTextField();
		panelRezyser.add(fieldRezyser);
		fieldRezyser.setColumns(10);

		JPanel panelRok = new JPanel();
		GridBagConstraints gbc_panelRok = new GridBagConstraints();
		gbc_panelRok.insets = new Insets(0, 0, 5, 0);
		gbc_panelRok.fill = GridBagConstraints.BOTH;
		gbc_panelRok.gridx = 0;
		gbc_panelRok.gridy = 3;
		panelLeftTop.add(panelRok, gbc_panelRok);
		panelRok.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel_2 = new JLabel("Rok:");
		panelRok.add(lblNewLabel_2);

		fieldRok = new JTextField();
		panelRok.add(fieldRok);
		fieldRok.setColumns(10);

		JPanel panelGatunek = new JPanel();
		GridBagConstraints gbc_panelGatunek = new GridBagConstraints();
		gbc_panelGatunek.insets = new Insets(0, 0, 5, 0);
		gbc_panelGatunek.fill = GridBagConstraints.BOTH;
		gbc_panelGatunek.gridx = 0;
		gbc_panelGatunek.gridy = 4;
		panelLeftTop.add(panelGatunek, gbc_panelGatunek);
		panelGatunek.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel_3 = new JLabel("Gatunek:");
		panelGatunek.add(lblNewLabel_3);

		fieldGatunek = new JTextField();
		panelGatunek.add(fieldGatunek);
		fieldGatunek.setColumns(10);

		JPanel panelOcena = new JPanel();
		GridBagConstraints gbc_panelOcena = new GridBagConstraints();
		gbc_panelOcena.fill = GridBagConstraints.BOTH;
		gbc_panelOcena.gridx = 0;
		gbc_panelOcena.gridy = 5;
		panelLeftTop.add(panelOcena, gbc_panelOcena);
		panelOcena.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel_4 = new JLabel("Ocena:");
		panelOcena.add(lblNewLabel_4);

		fieldOcena = new JTextField();
		panelOcena.add(fieldOcena);
		fieldOcena.setColumns(10);

		JPanel panelLeftBottom = new JPanel();
		GridBagConstraints gbc_panelLeftBottom = new GridBagConstraints();
		gbc_panelLeftBottom.fill = GridBagConstraints.BOTH;
		gbc_panelLeftBottom.gridx = 0;
		gbc_panelLeftBottom.gridy = 1;
		panelLeft.add(panelLeftBottom, gbc_panelLeftBottom);
		panelLeftBottom.setLayout(new GridLayout(0, 1, 0, 0));

		JButton add = new JButton("Dodaj");
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Stworzenie obiektu
				Movie m = new Movie();

				// ustawienie podstawowych pól
				m.setTitle(fieldTytul.getText());
				m.setDirector(fieldRezyser.getText());
				m.setYear(fieldRok.getText());
				m.setGenre(fieldGatunek.getText());

				// ustawienie pól konwertowanych (ocena to float)
				m.setRate(Float.parseFloat(fieldOcena.getText()));

				model.addMovie(m);
			}
		});
		panelLeftBottom.add(add);

		remove = new JButton("Usuń");
		remove.setEnabled(false);
		remove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				model.removeMovie(zaznaczonyIndex);
			}
		});
		panelLeftBottom.add(remove);

		JButton save = new JButton("Zapisz");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try (PrintWriter writer = new PrintWriter(new File("dane.txt"))) {
					for (Movie m : model.getListaFilmow()) {
						writer.println(m);
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		panelLeftBottom.add(save);

		JButton load = new JButton("Wczytaj");
		load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try (BufferedReader reader = new BufferedReader(new FileReader(new File("dane.txt")))) {
					String line = null;
					model.clear();
					while ((line = reader.readLine()) != null) {
						Movie m = new Movie(line);
						model.addMovie(m);
					}

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panelLeftBottom.add(load);

		JPanel panel_2 = new JPanel();
		panelGrid.add(panel_2);
		panel_2.setLayout(new GridLayout(1, 0, 0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel_2.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);
	}
}
