
public class Movie {
	private String title;
	private String director;
	private String year;
	private String genre;
	private float rate;

	public Movie() {
		super();
	}
	
	public Movie(String line){
		this.title = line.split(";;;")[0];
		this.director = line.split(";;;")[1];
		this.year = line.split(";;;")[2];
		this.genre = line.split(";;;")[3];
		this.rate = Float.parseFloat(line.split(";;;")[4]);
	}

	public Movie(String title, String director, String year, String genre, float rate) {
		super();
		this.title = title;
		this.director = director;
		this.year = year;
		this.genre = genre;
		this.rate = rate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public String toString() {
		return title + ";;;" + director + ";;;" + genre + ";;;" + year + ";;;" + rate;
	}
}
