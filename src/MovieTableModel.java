import java.util.LinkedList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class MovieTableModel extends AbstractTableModel {
	private final String[] columnNames = 
			new String[] { "Tytuł", "Reżyser", "Gatunek", "Ocena", "Rok" };

	private List<Movie> listaFilmow;

	public MovieTableModel() {
		super();
		this.listaFilmow = new LinkedList<>();
	}

	public List<Movie> getListaFilmow() {
		return listaFilmow;
	}
	
	public void clear(){
		listaFilmow.clear();
	}
	
	public void addMovie(Movie m) {
		listaFilmow.add(m);
		fireTableDataChanged();
	}
	
	public void removeMovie(int id){
		listaFilmow.remove(id);
		fireTableDataChanged();
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getRowCount() {
		return listaFilmow.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Movie tmp = listaFilmow.get(rowIndex);
		switch (columnIndex) {
		case 0: // co ma wyświetlić kolumna 0
			return tmp.getTitle();
		case 1: // co ma wyświetlić kolumna 1
			return tmp.getDirector();
		case 2: // co ma wyświetlić kolumna 2
			return tmp.getGenre();
		case 3: // co ma wyświetlić kolumna 3
			return tmp.getRate();
		case 4: // co ma wyświetlić kolumna 4
			return tmp.getYear();
		default: // co ma wyświetlić każda inna kolumna
			return "unknown";
		}
	}

}
